let sourceDir       = 'src';
let projectDir      = 'build';
let preprocessor    = 'scss';


let paths = {

    html: {
        src     : sourceDir + '/*.njk',
        dest    : projectDir + '/',
    },
    styles: {
        src     : sourceDir + '/scss/**/*.scss',
        dest    : projectDir + '/css/',
    },
    scripts: {
        src     : sourceDir + '/js/*.js',
        dest    : projectDir + '/js/',
    },
    images: {
        src     : [sourceDir + '/img/**/*.{jpg,png,gif,ico,webp}'],
        dest    : projectDir + '/img/',
    },
    svg: {
        src     : sourceDir + '/img/**/*.svg',
        dest    : projectDir + '/img/',
    },
    fonts: {
        src     : sourceDir + '/fonts/**/*.*',
        dest    : projectDir + '/fonts/',
    },
    lib: {
        js      : [
            'node_modules/jquery/dist/jquery.js',
            'node_modules/svg4everybody/dist/svg4everybody.legacy.js',
            'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js',
            'node_modules/swiper/swiper-bundle.js',
            'node_modules/jquery-ui-dist/jquery-ui.js',
            'node_modules/raty-js/lib/jquery.raty.js',
            'node_modules/ion-rangeslider/js/ion.rangeSlider.js',
            'libs/jquery.maskedinput/jquery.maskedinput.min.js',
            'node_modules/owl.carousel/dist/owl.carousel.js',
        ],
        css     : [
            'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.css',
            'node_modules/swiper/swiper-bundle.css',
            'node_modules/jquery-ui-dist/jquery-ui.css',
            'node_modules/ion-rangeslider/css/ion.rangeSlider.css',
            'node_modules/owl.carousel/dist/assets/owl.carousel.css'
        ]
    },
    watch: {
        html    : sourceDir + '/**/*.njk',
        css     : sourceDir + '/scss/**/*.scss',
        js      : sourceDir + '/js/**/*.js',
        svg     : sourceDir + '/img/**/*.svg',
        img     : sourceDir + '/img/**/*.{jpg,png,gif,ico,webp}'
    },

    clean: './' +  projectDir + '/'
}


const { src, dest, series, parallel } = require('gulp');
const gulp          = require('gulp');
const sass          = require('gulp-sass');
const cleanCSS      = require('gulp-clean-css');
const concat        = require('gulp-concat');
const rename        = require('gulp-rename');
const browsersync   = require('browser-sync');
const uglify        = require('gulp-uglify-es').default;
const autoprefixer  = require('gulp-autoprefixer');
const imagemin      = require('gulp-imagemin');
const newer         = require('gulp-newer');
const del           = require('del');
const sourcemaps    = require('gulp-sourcemaps');
const nunjucksRender = require('gulp-nunjucks-render');


function browserSync(params) {
    browsersync.init({
        server: {
            baseDir: './' +  projectDir + '/',
        },
        port: 4000,
        notify: false,
        online: true
    })
}


function html() {
    return src(paths.html.src)
        .pipe(nunjucksRender())
        .pipe(dest(paths.html.dest))
        .pipe(browsersync.stream())
}

gulp.task('nunjucks', function() {
    return gulp.src(paths.html.src)
        .pipe(nunjucksRender())
        .pipe(gulp.dest(paths.html.dest));
});

function scripts() {
    return src(paths.scripts.src)
        .pipe(dest(paths.scripts.dest))
        .pipe(
            uglify()
        )
        .pipe(
            rename({
                extname: '.min.js'
            })
        )
        .pipe(dest(paths.scripts.dest))
        .pipe(browsersync.stream())
}

function styles() {
    return gulp.src(paths.styles.src)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(
            sass({
                outputStyle: 'expanded'
            })
        )
        .pipe(
            autoprefixer({
                overrideBrowserslist: ['last 5 versions'],
                cascade: true
            })
        )
        .pipe(dest(paths.styles.dest))
        .pipe(cleanCSS())
        .pipe(
            rename({
                extname: '.min.css'
            })
        )
        .pipe(sourcemaps.write())
        .pipe(dest(paths.styles.dest))
        .pipe(browsersync.stream())
}

function images() {
	return src(paths.images.src)
	.pipe(newer(paths.images.dest))
	.pipe(imagemin())
	.pipe(dest(paths.images.dest))
}

function svg() {
    return src(paths.svg.src)
    .pipe(newer(paths.svg.dest))
    .pipe(dest(paths.svg.dest))
}

function fonts() {
    return src(paths.fonts.src)
        .pipe(dest(paths.fonts.dest))
        .pipe(newer(paths.fonts.dest))
}

function libsJS() {
    return src(paths.lib.js)
        .pipe(concat('libs.js'))
        .pipe(dest(paths.scripts.dest))
        .pipe(
            uglify()
        )
        .pipe(
            rename({
                extname: '.min.js'
            })
        )
        .pipe(dest(paths.scripts.dest))
}

function libsCSS() {
    return src(paths.lib.css)
        .pipe(concat('libs.css'))
        .pipe(dest(paths.styles.dest))
        .pipe(cleanCSS())
        .pipe(
            rename({
                extname: '.min.css'
            })
        )
        .pipe(dest(paths.styles.dest))
}



function clean(params) {
    return del(paths.clean);
}

function watchFiles(params) {
    gulp.watch([paths.watch.html],html);
    gulp.watch([paths.watch.css],styles);
    gulp.watch([paths.watch.js],scripts);
    gulp.watch([paths.watch.img],images);
    gulp.watch([paths.watch.svg],svg);
}

exports.clean       = clean;
exports.html        = html;
exports.styles      = styles;
exports.scripts     = scripts;
exports.images      = images;
exports.svg         = svg;
exports.fonts       = fonts;
exports.libsJS      = libsJS;
exports.libsCSS     = libsCSS;
exports.build       = series(styles, scripts, images, html, fonts, svg, libsJS, libsCSS);
exports.default     = parallel(watchFiles, browserSync);
exports.njk         = series(html);