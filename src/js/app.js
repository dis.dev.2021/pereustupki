'use strict';

document.addEventListener("DOMContentLoaded", function() {

    let isFlats =       $('div').is('.flat__header');
    let mainScreenWidth = document.body.clientWidth;

    // SVG IE11 support
    svg4everybody();
    
    // Mask input
    $("input[name='phone']").mask("+7(999) 999-9999");

    // Mobile Nav Toggle
    $('.nav-toggle').on('click', function(e){
        e.preventDefault();
        $('.page').toggleClass('nav-open');
    });

    // Modal 
    $('.btn-modal').fancybox({
        autoFocus: false,
    });


    $('.block__nav li a').on('click', function(e){
        e.preventDefault();
        let blockNav = $(this).closest('.block__nav');
        blockNav.find('li').removeClass('active');
        $(this).closest('li').addClass('active');
    });

    $('.raty').raty({
        number: 5,
        path          : 'img',
        starHalf      : 'star__half.svg',
        starOff       : 'star__off.svg',
        starOn        : 'star__on.svg',
        cancelOff     : 'cancel-off.png',
        cancelOn      : 'cancel-on.png',
        score: function() {
            return $(this).attr('data-score');
        },
        readOnly: function() {
            return $(this).attr('data-readOnly');
        },
    });

    $('.accordion-toggle').on('click', function(e){
        e.preventDefault();
        console.log('click');
        let accordion = $(this).closest('.accordion')
        accordion.toggleClass('open');
        accordion.find('.accordion__content').slideToggle('fast');
    });

    $(".btn-scroll").on("click", function(e) {
        e.preventDefault();
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top -100;
        $('html, body').animate({ scrollTop: destination }, 600);
        return false;
    });
    // Placeholders

    $('.field__input').on('focus', function(event) {
        $(this).closest('.field').addClass('focus');
    });

    $('.field__input').on('focusout', function(){

        var inputVal = $(this).closest('.field').find('.field__input').val();
        if (inputVal == '') {
            $(this).closest('.field').removeClass('focus');
        }
    });

    $("input[name='phone']").mask("+7(999) 999-9999");

    let caseSlider = new Swiper('.case-slider',{
        loop: true,
        slidesPerView: 1,
        spaceBetween: 20,
        navigation: {
            nextEl: '.case-next',
            prevEl: '.case-prev',
        },
        breakpoints: {
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            1440: {
                slidesPerView: 3,
                spaceBetween: 50,
            },
        }
    });

    let developerSlider = new Swiper('.developer-slider',{
        loop: true,
        slidesPerView: 1,
        spaceBetween: 20,
        navigation: {
            nextEl: '.developer-next',
            prevEl: '.developer-prev',
        },
        breakpoints: {
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            1024: {
                slidesPerView: 4,
                spaceBetween: 30,
            },
            1440: {
                slidesPerView: 4,
                spaceBetween: 50,
            },
        }
    });

    let articleSlider = new Swiper('.article-slider',{
        loop: true,
        slidesPerView: 1,
        spaceBetween: 20,
        breakpoints: {
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            1440: {
                slidesPerView: 3,
                spaceBetween: 50,
            },
        }
    });

    let newsSlider = new Swiper('.news-slider',{
        loop: true,
        slidesPerView: 1,
        spaceBetween: 20,
        breakpoints: {
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            1024: {
                slidesPerView: 4,
                spaceBetween: 30,
            },
            1440: {
                slidesPerView: 4,
                spaceBetween: 50,
            },
        }
    });

    let flatsSlider = new Swiper('.flats-slider',{
        loop: true,
        slidesPerView: 1,
        spaceBetween: 20,
        navigation: {
            nextEl: '.flats-next',
            prevEl: '.flats-prev',
        },
        breakpoints: {
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            1024: {
                slidesPerView: 4,
                spaceBetween: 30,
            },
            1440: {
                slidesPerView: 4,
                spaceBetween: 50,
            },
        }
    });


    // Sort Button

    $('.btn-sort').on("click", function(e) {
        let check = $(this).hasClass('active');
        if(check) {
            $(this).toggleClass('invert');
        }
        else {
            $(this).addClass('active');
        }
    });

    $('.mobile-sort').on("click", function(e) {
        let check = $(this).hasClass('active');
        if(check) {
            $(this).toggleClass('invert');
        }
        else {
            $(this).addClass('active');
        }
    });

    // Filter

    $(".filter-sm-toggle").on("click", function(e) {
        e.preventDefault();
        $('.filter-sm__advance').slideToggle('fast');
    });

    $(".btn-filter-toggle").on("click", function(e) {
        e.preventDefault();
        $('body').toggleClass('filter-open');
        $('.filter-mobile').toggleClass('open');
    });

    $(".filter-mobile-good").on("click", function(e) {
        e.preventDefault();
        $('.filter-mobile').removeClass('open');
    });

    $(".filter-mobile-clean").on("click", function(e) {
        e.preventDefault();
        $('.filter-mobile').removeClass('open');
    });

    $('.filter-range__input').on('focus', function(event) {
        $(this).closest('.filter-range').addClass('focus');
    });

    $('.filter-range__input').on('focusout', function(){
        $(this).closest('.filter-range').removeClass('focus');
        //	$(this).closest('.filter-range__field').removeClass('focus');

        var inputVal = $(this).closest('.field').find('.field__input').val();
        if (inputVal != '') {
            $(this).closest('.filter-range__field').addClass('completed');
        }
    });

    $('.filter-range__clean').on('click', function(e) {
        e.preventDefault();
        $(this).closest('.filter-range__field').find('.filter-range__input').val('');
        $(this).closest('.filter-range__field').removeClass('completed');
    });

    $('.field-control__input').on('focusout', function(){
        var inputVal = $(this).val();
        if (inputVal != '') {
            $(this).closest('.field-control').addClass('completed');
        }
    });

    $('.field-control__clean').on('click', function(e) {
        e.preventDefault();
        $(this).closest('.field-control').find('.field-control__input').val('');
        $(this).closest('.field-control').removeClass('completed');
    });

    $('.filter-toggle').on('click', function(e) {
        e.preventDefault();
        $(this).toggleClass('open')
        $('.filter__second').slideToggle('fast');
    });

    $('.filter-modal__list li').on('click', function(e) {
        e.preventDefault();
        $(this).toggleClass('selected');
    });

    if(isFlats) {
        let $h = $('.flat__header').offset().top;

        $(window).scroll(function(){
            let $ff = $(window).scrollTop() - $('.flat__footer').offset().top;

            if ( $(window).scrollTop() > $h) {
                $('.flat__header').addClass('fix-top');
            }else{
                $('.flat__header').removeClass('fix-top');
            }

            if($ff > 0) {
                $('.flat__header').addClass('hide');
                $('.flat__header').removeClass('fix-top');
            }
            else {
                $('.flat__header').removeClass('hide');
            }
        });
    }

    $('.filter-range__input').bind("change keyup input click", function() {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9.]/g,'').replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        }
    });

    $('.offer-form-send').on('click', function(e) {
        e.preventDefault();
        $(this).closest('.offer-form').addClass('successfully');
    });

    let filterTags = new Swiper('.filter-tags', {
        slidesPerView: 'auto',
        spaceBetween: 20,
        loop: true,
        navigation: {
            nextEl: '.filter__tags--next',
            prevEl: '.filter__tags--prev',
        },
    });

    // Footer Nav
    $('.footer-nav__header').on('click', function(e){
        e.preventDefault();
        $(this).closest('.footer-nav__group').toggleClass('open');
    });

    $('.footer-nav__view').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('open');
        $(this).closest('.footer-nav__content').find('.footer-nav__menu').toggleClass('open');
    });


    // Metro Map

    $('.geo-filter-toggle').on('click', function(e) {
        e.preventDefault();
        $('.geo-filter').toggleClass('open');
    });

    $('.geo-filter__nav--item').on('click', function(e) {
        e.preventDefault();
        let geo_tab = $(this).attr('href');
        $('.geo-filter__nav--item').removeClass('active');
        $(this).addClass('active');

        $('.geo-filter__body').removeClass('active');
        $(geo_tab).addClass('active');
    });


    $('.metro-station').on('click', function(e) {
        e.preventDefault();
        $(this).toggleClass('checked');
    });

    $('.metro-line-num').on('click', function(e) {
        e.preventDefault();
        let metroLineCheck	= $(this).hasClass('selected');
        let metroLineNum	= $(this).attr('data-line');
        let metroLineClass	= '.metro-station__line-' + metroLineNum;
        if(metroLineCheck) {
            $(this).removeClass('selected');
            $(metroLineClass).removeClass('checked');
        }
        else {
            $(this).addClass('selected');
            $(metroLineClass).addClass('checked');
        }
    });

    $('.metro-line-num').on('mouseenter', function(e) {
        e.preventDefault();
        let metroLineNum	= $(this).attr('data-line');
        let metroLineClass	= '.metro-station__line-' + metroLineNum;
        $(metroLineClass).addClass('hover');
        console.log(metroLineClass);
    });

    $('.metro-line-num').on('mouseleave', function(e) {
        e.preventDefault();
        let metroLineNum	= $(this).attr('data-line');
        let metroLineClass	= '.metro-station__line-' + metroLineNum;
        $(metroLineClass).removeClass('hover');
        console.log(metroLineClass);
    });

    $( ".metro__main" ).draggable();

    $('.scale-plus').on('click', function(e) {
        e.preventDefault();
        let mapScale = parseFloat($('.metro__scale').attr('data-scale'));
        if (mapScale < 1.4) {
            mapScale = mapScale + 0.1;
            $('.metro__scale').attr('data-scale',mapScale);
            let mapScaleStyle   = 'scale(' + mapScale + ')';
            $('.metro__map').css('transform', mapScaleStyle);
        }
    });

    $('.scale-minus').on('click', function(e) {
        e.preventDefault();
        let mapScale = parseFloat($('.metro__scale').attr('data-scale'));
        console.log(mapScale);
        if (mapScale > 0.7) {
            mapScale = mapScale - 0.1;
            $('.metro__scale').attr('data-scale',mapScale);
            let mapScaleStyle   = 'scale(' + mapScale + ')';
            $('.metro__map').css('transform', mapScaleStyle);
        }
    });

    $('.main-select__item').on("click", function(e) {
        let selectSource	= $(this).html();
        let select 			= $($(this).closest('.main-select'));
        let metroLineNum	= $(this).attr('data-value');
        let metroLineClass	= '.metro-station__line-' + metroLineNum;
        select.removeClass('open')
        select.find('.main-select__button').html(selectSource);
        select.find('.main-select__item').removeClass('selected');
        $(this).addClass('selected');

        $('.metro-station').removeClass('checked');
        $(metroLineClass).addClass('checked');
    });

    $('.main-select__button').on("click", function(e) {
        let check = $(this).closest('.main-select').hasClass('open');
        if(check) {
            $(this).closest('.main-select').removeClass('open');
        }
        else {
            $('.main-select').removeClass('open');
            $(this).closest('.main-select').addClass('open');
        }
    });

    $('body').click(function (event) {

        if ($(event.target).closest(".main-select").length === 0) {
            $(".main-select").removeClass('open');
        }

        if ($(event.target).closest(".object-tabs__nav").length === 0) {
            $(".object-tabs__nav").removeClass('open');
        }

        if ($(event.target).closest(".object-slide-point").length === 0) {
            $(".object-slide-point").removeClass('open');
        }
    });

    let mediaSlider = new Swiper('.media-slider', {
        loop: true,
        navigation: {
            nextEl: '.media-next',
            prevEl: '.media-prev',
        },
        pagination: {
            el: '.slider-pagination',
        },
    })

    $('.discussion__toggle').on('click', function(e) {
        e.preventDefault();
        $(this).closest('.discussion').toggleClass('open');
    });

    const objectMediaThumbs = new Swiper('.object-media-thumbs', {
        loop: false,
        slidesPerView: "auto",
        spaceBetween: 15,
    });

    const objectMediaSlider = new Swiper('.object-media-gallery', {
        loop: false,
        spaceBetween: 10,
        breakpoints: {
            768: {
                spaceBetween: 30,
            }
        },
        thumbs: {
            swiper: objectMediaThumbs,
        },
    });

    $('.object-info--toggle').on("click", function(e) {
        e.preventDefault();
        $(this).closest('.object-info').find('.object-info__params').toggleClass('open');
        $(this).toggleClass('open');
    });

    let objectPromoSlider = new Swiper('.object-promo-slider',{
        loop: false,
        slidesPerView: 1,
        spaceBetween: 20,
        navigation: {
            nextEl: '.object-promo-next',
            prevEl: '.object-promo-prev',
        },
        breakpoints: {
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 30,
            }
        }
    });

    // Object tab

    let $tabSlider = $('.object-tabs-slider');

    $tabSlider.owlCarousel({
        items: 1,
        margin: 30,
        dots: false,
    });

    $('.tab-slider-prev').on('click', function(e) {
        e.preventDefault();
        $tabSlider.trigger('prev.owl.carousel');
    })

    $('.tab-slider-next').on('click', function(e) {
        e.preventDefault();
        $tabSlider.trigger('next.owl.carousel');
    })


    $('.object-tabs__nav--button').on("click", function(e) {
        e.preventDefault();
        $(this).closest('.object-tabs__nav').toggleClass('open');
    });

    $('.object-tabs__nav--item').on("click", function(e) {
        e.preventDefault();
        let navItem         = $(this);
        let navItemName     = $(this).text();
        let tabsNav         = navItem.closest('.object-tabs__nav');
        let tabs            = navItem.closest('.object-tabs');
        let tabsItem        = $(this).attr('data-target');

        tabsNav.removeClass('open');
        tabsNav.find('.object-tabs__nav--item').removeClass('active');
        navItem.addClass('active');
        tabsNav.find('.object-tabs__nav--active').text(navItemName);

        tabs.find('.object-tabs__item').removeClass('active');
        tabs.find(tabsItem).addClass('active');

        $tabSlider.trigger('refresh.owl.carousel');

    });

    // Credit Calc

    let $rangePriceSlider   = $('.credit-price-slider');
    let $rangePaymentSlider = $('.credit-payment-slider');
    let $rangeTermSlider = $('.credit-term-slider');
    let $rangePriceLabel = $('.credit-price-value');
    let $rangePaymentLabel = $('.credit-payment-value');
    let $rangeTermLabel = $('.credit-term-value');
    let $rangePercentLabel = $('.credit-percent-value');

    $rangePriceSlider.ionRangeSlider({
        skin: 'round',
        grid: false,
    });

    $rangePaymentSlider.ionRangeSlider({
        skin: 'round',
        grid: false,
    });

    $rangeTermSlider.ionRangeSlider({
        skin: 'round',
        grid: false,
    });

    $rangePriceSlider.on("change", function () {
        let price = $(this).prop("value");
        $rangePriceLabel.text(String(price).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 '));
    });

    $rangePaymentSlider.on("change", function () {
        let first_payment = $(this).prop("value");
        let price = $rangePriceSlider.prop("value");
        let percent = ((first_payment * 100 )/ price).toFixed(1)
        $rangePercentLabel.text(percent);
        $rangePaymentLabel.text(String(first_payment).replace(/(\d)(?=(\d{3})+([^\d]|$))/g, '$1 '));
    });

    $rangeTermSlider.on("change", function () {
        let term = $(this).prop("value");
        $rangeTermLabel.text(term);
    });

    $('.object-credit__view').on("click", function(e) {
        e.preventDefault();
        $(this).closest('.object-credit').find('.object-credit__table').toggleClass('open');
        $(this).toggleClass('open');
    });

    const objectLocationPlaces = new Swiper('.object-location-places',{
        loop: false,
        slidesPerView: 1,
        spaceBetween: 20,
        navigation: {
            nextEl: '.object-location-next',
            prevEl: '.object-location-prev',
        },
        breakpoints: {
            576: {
                slidesPerView: 2,
                spaceBetween: 30,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
            }
        }
    });

    $('.object-accordion__header').on('click', function(e){
        e.preventDefault();
        let block = $(this).closest('.object-accordion');
        let accordion = $(this).closest('.object-accordion__item');
        if (accordion.hasClass('open')) {
            accordion.removeClass('open');
            accordion.find('.object-accordion__content').slideUp('fast');
        }
        else {

            block.find('.object-accordion__item').removeClass('open');
            block.find('.object-accordion__content').slideUp('fast');
            accordion.addClass('open');
            accordion.find('.object-accordion__content').slideDown('fast');
        }
    });


    let $objectProgressSlider = $(".object-progress-slider");

    $objectProgressSlider.owlCarousel({
        items: 1,
        margin: 10,
        loop: false,
        dots: false,
        navigation: {
            nextEl: '.object-progress-next',
            prevEl: '.object-progress-prev',
        },
        responsive : {
            0 : {
                items: 1,
                margin: 10,
            },
            567 : {
                items: 2,
                margin: 16,
            },
            768 : {
                items: 2,
                margin: 30,
            },
            1024 : {
                items: 3,
                margin: 30,
            }
        }
    });

    $('.object-progress-prev').on('click', function(e) {
        e.preventDefault();
        $objectProgressSlider.trigger('prev.owl.carousel');
    })

    $('.object-progress-next').on('click', function(e) {
        e.preventDefault();
        $objectProgressSlider.trigger('next.owl.carousel');
    });

    $('.docs-list__toggle').on("click", function(e) {
        e.preventDefault();
        $(this).closest('.docs-list').toggleClass('open');
        $(this).toggleClass('open');
    });

    const objectCollectionSlider = new Swiper('.object-collection-slider',{
        loop: false,
        slidesPerView: 1,
        spaceBetween: 20,
        navigation: {
            nextEl: '.object-collection-next',
            prevEl: '.object-collection-prev',
        },
        breakpoints: {
            576: {
                slidesPerView: 2,
                spaceBetween: 30,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 30,
            }
        }
    });

    const objectComplexSlider = new Swiper('.object-complex-slider',{
        loop: false,
        slidesPerView: 2,
        spaceBetween: 10,
        navigation: {
            nextEl: '.object-complex-next',
            prevEl: '.object-complex-prev',
        },
        breakpoints: {
            576: {
                slidesPerView: 2,
                spaceBetween: 30,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 30,
            }
        }
    });

    const objectArticleSlider = new Swiper('.object-article-slider',{
        loop: false,
        slidesPerView: 1,
        spaceBetween: 20,
        navigation: {
            nextEl: '.object-article-next',
            prevEl: '.object-article-prev',
        },
        breakpoints: {
            576: {
                slidesPerView: 2,
                spaceBetween: 30,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30,
            }
        }
    });

    $('.object-slide-point').on("click", function(e) {
        e.preventDefault();
        let point = $(this);
        let slide = $(this).closest('.object-slide');
        if(point.hasClass('open')) {
            point.removeClass('open');
        }
        else {
            slide.find('.object-slide-point').removeClass('open');
            point.addClass('open');
        }
    });


    $('.contact-block-toggle').on("click", function(e) {
        e.preventDefault();
        $('.contact-block').toggleClass('open');
    });

    if (mainScreenWidth > 1229) {

    }

    const journalBlockSlider = new Swiper('.journal-block-slider',{
        loop: false,
        slidesPerView: "auto",
        spaceBetween: 15,
        navigation: {
            nextEl: '.journal-block-next',
            prevEl: '.journal-block-prev',
        },
        breakpoints: {
            576: {
                spaceBetween: 30,
            },
            768: {
                spaceBetween: 30,
            }
        }
    });

    // Post nav
    $('.post-nav__header').on("click", function(e) {
        e.preventDefault();
        $('.post-nav').toggleClass('open');
    });

    $('.post-nav__link').on("click", function(e) {
        e.preventDefault();
        $('.post-nav').removeClass('open');
    });

    const postMediaThumbs = new Swiper('.post-media-thumbs', {
        loop: false,
        slidesPerView: "auto",
        spaceBetween: 20,
    });

    const postMediaGallery = new Swiper('.post-media-gallery',{
        loop: false,
        slidesPerView: 1,
        spaceBetween: 15,
        thumbs: {
            swiper: postMediaThumbs,
        },
    });

    $('.contact-map-toggle').on("click", function(e) {
        e.preventDefault();
        $('.contact__map').toggleClass('open');
    });

    (function(){

        if (mainScreenWidth > 1229) {

            var a = document.querySelector('#sticky-block'), b = null, K = null, Z = 0, P = 0, N = 0;  // если у P ноль заменить на число, то блок будет прилипать до того, как верхний край окна браузера дойдёт до верхнего края элемента, если у N — нижний край дойдёт до нижнего края элемента. Может быть отрицательным числом
            window.addEventListener('scroll', Ascroll, false);
            document.body.addEventListener('scroll', Ascroll, false);
            function Ascroll() {
                var Ra = a.getBoundingClientRect(),
                    R1bottom = document.querySelector('#content-block').getBoundingClientRect().bottom;
                if (Ra.bottom < R1bottom) {
                    if (b == null) {
                        var Sa = getComputedStyle(a, ''), s = '';
                        for (var i = 0; i < Sa.length; i++) {
                            if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                                s += Sa[i] + ': ' +Sa.getPropertyValue(Sa[i]) + '; '
                            }
                        }
                        b = document.createElement('div');
                        b.className = "stop";
                        b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
                        a.insertBefore(b, a.firstChild);
                        var l = a.childNodes.length;
                        for (var i = 1; i < l; i++) {
                            b.appendChild(a.childNodes[1]);
                        }
                        a.style.height = b.getBoundingClientRect().height + 'px';
                        a.style.padding = '0';
                        a.style.border = '0';
                    }
                    var Rb = b.getBoundingClientRect(),
                        Rh = Ra.top + Rb.height,
                        W = document.documentElement.clientHeight,
                        R1 = Math.round(Rh - R1bottom),
                        R2 = Math.round(Rh - W);
                    if (Rb.height > W) {
                        if (Ra.top < K) {  // скролл вниз
                            if (R2 + N > R1) {  // не дойти до низа
                                if (Rb.bottom - W + N <= 0) {  // подцепиться
                                    b.className = 'sticky';
                                    b.style.top = W - Rb.height - N + 'px';
                                    Z = N + Ra.top + Rb.height - W;
                                } else {
                                    b.className = 'stop';
                                    b.style.top = - Z + 'px';
                                }
                            } else {
                                b.className = 'stop';
                                b.style.top = - R1 +'px';
                                Z = R1;
                            }
                        } else {  // скролл вверх
                            if (Ra.top - P < 0) {  // не дойти до верха
                                if (Rb.top - P >= 0) {  // подцепиться
                                    b.className = 'sticky';
                                    b.style.top = P + 'px';
                                    Z = Ra.top - P;
                                } else {
                                    b.className = 'stop';
                                    b.style.top = - Z + 'px';
                                }
                            } else {
                                b.className = '';
                                b.style.top = '';
                                Z = 0;
                            }
                        }
                        K = Ra.top;
                    } else {
                        if ((Ra.top - P) <= 0) {
                            if ((Ra.top - P) <= R1) {
                                b.className = 'stop';
                                b.style.top = - R1 +'px';
                            } else {
                                b.className = 'sticky';
                                b.style.top = P + 'px';
                            }
                        } else {
                            b.className = '';
                            b.style.top = '';
                        }
                    }
                    window.addEventListener('resize', function() {
                        a.children[0].style.width = getComputedStyle(a, '').width
                    }, false);
                }
            }
        }
    })()

});

